package com.nagarro.nagp.urbanclap.servicecatalog.service;

import com.nagarro.nagp.urbanclap.servicecatalog.domain.model.request.CreateProductServiceRequest;
import com.nagarro.nagp.urbanclap.servicecatalog.domain.model.response.BaseResponse;
import com.nagarro.nagp.urbanclap.servicecatalog.domain.model.response.ProductServiceResponse;
import com.nagarro.nagp.urbanclap.servicecatalog.domain.model.response.ProductServicesListResponse;

public interface ProductService {

    ProductServicesListResponse getAllProducts();

    ProductServiceResponse getProductById(String serviceId);

    ProductServiceResponse createProductService(CreateProductServiceRequest createProductServiceRequest);

    ProductServiceResponse updateProductService(String serviceId, CreateProductServiceRequest createProductServiceRequest);

    BaseResponse removeProductService(String serviceId);

}
