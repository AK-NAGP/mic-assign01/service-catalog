package com.nagarro.nagp.urbanclap.servicecatalog.domain.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductServicesListResponse extends BaseResponse {

    private List<ProductServiceResponse> services;

    public ProductServicesListResponse(String status, String message, List<ProductServiceResponse> products) {
        super(status, message);
        this.services = products;
    }
}
