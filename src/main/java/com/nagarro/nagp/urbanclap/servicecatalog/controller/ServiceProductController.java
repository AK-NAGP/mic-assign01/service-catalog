package com.nagarro.nagp.urbanclap.servicecatalog.controller;

import com.nagarro.nagp.urbanclap.servicecatalog.domain.model.request.CreateProductServiceRequest;
import com.nagarro.nagp.urbanclap.servicecatalog.domain.model.response.BaseResponse;
import com.nagarro.nagp.urbanclap.servicecatalog.domain.model.response.ProductServiceResponse;
import com.nagarro.nagp.urbanclap.servicecatalog.domain.model.response.ProductServicesListResponse;
import com.nagarro.nagp.urbanclap.servicecatalog.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping("services")
public class ServiceProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("all")
    public ResponseEntity<ProductServicesListResponse> getAllProducts() {
        return ResponseEntity.ok(productService.getAllProducts());
    }

    @GetMapping("{serviceId}")
    public ResponseEntity<ProductServiceResponse> getProductById(
            @PathVariable("serviceId") String serviceId
    ) {
        return ResponseEntity.ok(productService.getProductById(serviceId));
    }

    @PostMapping()
    public ResponseEntity<ProductServiceResponse> createProduct(
            @RequestBody CreateProductServiceRequest createProductServiceRequest
    ) {
        return ResponseEntity.ok(productService.createProductService(createProductServiceRequest));
    }

    @PutMapping("{serviceId}")
    public ResponseEntity<ProductServiceResponse> updateProduct(
            @PathVariable("serviceId") String serviceId,
            @RequestBody CreateProductServiceRequest createProductServiceRequest
    ) {
        return ResponseEntity.ok(productService.updateProductService(serviceId,createProductServiceRequest));
    }

    @DeleteMapping("{serviceId}")
    public ResponseEntity<BaseResponse> deleteProduct(
            @PathVariable("serviceId") String serviceId
    ) {
        return ResponseEntity.ok(productService.removeProductService(serviceId));
    }


}
