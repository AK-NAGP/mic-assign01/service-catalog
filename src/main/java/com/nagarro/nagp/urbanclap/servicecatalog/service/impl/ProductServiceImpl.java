package com.nagarro.nagp.urbanclap.servicecatalog.service.impl;

import com.nagarro.nagp.urbanclap.servicecatalog.domain.entity.ProductServiceEntity;
import com.nagarro.nagp.urbanclap.servicecatalog.domain.model.request.CreateProductServiceRequest;
import com.nagarro.nagp.urbanclap.servicecatalog.domain.model.response.BaseResponse;
import com.nagarro.nagp.urbanclap.servicecatalog.domain.model.response.ProductServiceResponse;
import com.nagarro.nagp.urbanclap.servicecatalog.domain.model.response.ProductServicesListResponse;
import com.nagarro.nagp.urbanclap.servicecatalog.repository.ProductServiceRepository;
import com.nagarro.nagp.urbanclap.servicecatalog.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductServiceRepository productServiceRepository;

    @Override
    public ProductServicesListResponse getAllProducts() {
        List<ProductServiceEntity> productServiceList = productServiceRepository.findAll();
        List<ProductServiceResponse> products = productServiceList
                .stream()
                .map(this::mapProductEntityToResponse)
                .collect(Collectors.toList());

        return new ProductServicesListResponse("success", "All Services List", products);
    }

    @Override
    public ProductServiceResponse getProductById(String serviceId) {
        Optional<ProductServiceEntity> entityOptional = productServiceRepository.findById(Integer.parseInt(serviceId));
        return entityOptional
                .map(this::mapProductEntityToResponse)
                .orElseGet(() -> new ProductServiceResponse("failure", "Service Not Found"));
    }

    @Override
    public ProductServiceResponse createProductService(CreateProductServiceRequest createProductServiceRequest) {
        ProductServiceEntity newEntity = new ProductServiceEntity();
        BeanUtils.copyProperties(createProductServiceRequest, newEntity);
        this.productServiceRepository.save(newEntity);

        return this.mapProductEntityToResponse(newEntity);
    }

    @Override
    public ProductServiceResponse updateProductService(String serviceId, CreateProductServiceRequest createProductServiceRequest) {
        Optional<ProductServiceEntity> entityOptional = productServiceRepository.findById(Integer.parseInt(serviceId));
        if (entityOptional.isPresent()) {
            ProductServiceEntity productServiceEntity = entityOptional.get();
            BeanUtils.copyProperties(createProductServiceRequest, productServiceEntity);
            productServiceEntity.setId(Integer.parseInt(serviceId));
            productServiceEntity.setIsServiceAvailable(createProductServiceRequest.isServiceAvailable());

            this.productServiceRepository.save(productServiceEntity);

            return this.mapProductEntityToResponse(productServiceEntity);
        } else {
            return new ProductServiceResponse("failure", "Product Not Found");
        }
    }

    @Override
    public BaseResponse removeProductService(String serviceId) {
        Optional<ProductServiceEntity> entityOptional = productServiceRepository.findById(Integer.parseInt(serviceId));
        if (entityOptional.isPresent()) {
            this.productServiceRepository.delete(entityOptional.get());
            return new BaseResponse("success", serviceId + " deleted successfully");
        } else {
            return new BaseResponse("failure", "Product Not Found");
        }
    }

    private ProductServiceResponse mapProductEntityToResponse(ProductServiceEntity productServiceEntity) {
        ProductServiceResponse productServiceResponse = new ProductServiceResponse();
        BeanUtils.copyProperties(productServiceEntity, productServiceResponse);
        return productServiceResponse;
    }
}
