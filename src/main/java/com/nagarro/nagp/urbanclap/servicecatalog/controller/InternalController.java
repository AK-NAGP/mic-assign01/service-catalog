package com.nagarro.nagp.urbanclap.servicecatalog.controller;

import com.nagarro.nagp.urbanclap.servicecatalog.domain.model.response.ProductServiceResponse;
import com.nagarro.nagp.urbanclap.servicecatalog.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("internal/services")
public class InternalController {

    @Autowired
    private ProductService productService;

    @GetMapping("{serviceId}")
    public ResponseEntity<ProductServiceResponse> getProductById(
            @PathVariable("serviceId") String serviceId
    ) {
        return ResponseEntity.ok(productService.getProductById(serviceId));
    }
}
