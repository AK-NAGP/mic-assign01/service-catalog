package com.nagarro.nagp.urbanclap.servicecatalog.domain.entity;

import com.google.common.base.MoreObjects;
import com.nagarro.nagp.urbanclap.servicecatalog.domain.enums.ServiceTypeEnum;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Entity(name = "PRODUCT_SERVICE")
@Data
@ToString
public class ProductServiceEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String serviceName;

    @Column
    @Enumerated(EnumType.STRING)
    private ServiceTypeEnum serviceType;

    @Column
    private Double charges;

    @Column
    private Boolean isServiceAvailable = true;

}
