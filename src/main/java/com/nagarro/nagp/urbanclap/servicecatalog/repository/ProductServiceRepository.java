package com.nagarro.nagp.urbanclap.servicecatalog.repository;

import com.nagarro.nagp.urbanclap.servicecatalog.domain.entity.ProductServiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductServiceRepository extends JpaRepository<ProductServiceEntity, Integer> {
}
