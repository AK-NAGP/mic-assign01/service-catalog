package com.nagarro.nagp.urbanclap.servicecatalog.domain.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.nagarro.nagp.urbanclap.servicecatalog.domain.enums.ServiceTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductServiceResponse extends BaseResponse {

    private Integer id;

    private String serviceName;

    private ServiceTypeEnum serviceType;

    private Double charges;

    private Boolean isServiceAvailable;

    public ProductServiceResponse(String status, String message) {
        super(status, message);
    }
}
