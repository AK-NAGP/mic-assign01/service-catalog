package com.nagarro.nagp.urbanclap.servicecatalog.domain.enums;

public enum ServiceTypeEnum {
    ELECTRICIAN,
    YOGA_TRAINER,
    INTERIOR_DESIGNER,
    PLUMBER,
    HAIR_DRESSER,
    PAINTER;
}
