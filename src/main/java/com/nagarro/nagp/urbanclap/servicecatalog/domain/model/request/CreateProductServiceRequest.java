package com.nagarro.nagp.urbanclap.servicecatalog.domain.model.request;

import com.nagarro.nagp.urbanclap.servicecatalog.domain.enums.ServiceTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateProductServiceRequest {

    private String serviceName;

    private ServiceTypeEnum serviceType;

    private double charges;

    private boolean isServiceAvailable;

}
